import { Square } from "./module/Figure/Square";
import { Rectangle } from "./module/Figure/Rectangle";
import { Triangle } from "./module/Figure/Triangle";
import { Print } from "./module/Print";
import { Figure } from "./module/Figure/Figure";

try {    
    const square = new Square(10);
    const rectangle = new Rectangle(10, 2);
    const triangle = new Triangle(4, 4, 5);

    const figures: Figure[] = [square, rectangle, triangle];

    const printer = new Print(figures);
    
    printer.console();

} catch (e) {
    console.log(e);
}
