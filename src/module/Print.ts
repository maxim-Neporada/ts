import { Figure } from "./Figure/Figure";

export class Print {
    constructor(protected figures: Figure[]) {
        this.figures = figures
    }

    console() {
        for (const figure of this.figures) {
            console.log(`Ответ: площадь ${figure.figureType}a равен =  ${figure.area()}; \n периметр ${figure.figureType}a равен =  ${figure.area()} \n\n `);
        }
    }
}