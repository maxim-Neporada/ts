export abstract class Figure {
    public abstract figureType: string;
    protected abstract validate(): void;
    public abstract area(): number;
    public abstract perimeter(): number;
}