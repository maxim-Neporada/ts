import {Figure} from "./Figure";

export class Rectangle extends Figure {
    figureType = "Прямоугольник";

    constructor(protected a: number, protected b: number) {
        super();
        this.a = a;
        this.b = b;
        this.validate();
    }

    protected validate(): void {
        if (this.a <= 0 || this.b <= 0) throw new Error(`${this.figureType}. Вы  ввели отрицательное число или ноль`);
    }

    public area(): number {
        return this.a * this.b;
    }

    public perimeter(): number {
        return 2 * (this.a + this.b);
    }
}