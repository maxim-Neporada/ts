import {Figure} from "./Figure";

export class Square extends Figure {
    figureType = "Квадрат";

    constructor(protected a: number) {
        super();
        this.a = a;
        this.validate();
    }

    protected validate(): void {
        if (this.a <= 0) throw new Error(`${this.figureType}. Вы  ввели отрицательное число или ноль`);
    }

    public area(): number {
        return Math.pow(this.a, 2);
    }

    public perimeter(): number {
        return 4 * this.a;
    }
}