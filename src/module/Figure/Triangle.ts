import {Figure} from "./Figure";

export class Triangle extends Figure {
    figureType = "Треугольник"

    constructor(protected a: number, protected b: number, protected c: number) {
        super();
        this.a = a;
        this.b = b;
        this.c = c;
        this.validate();
    }

    protected validate(): void {
        if (this.a <= 0 || this.b <= 0 || this.c <= 0) throw new Error(`${this.figureType}. Вы  ввели отрицательное число или ноль`);
        if (!(this.a + this.b > this.c && this.a + this.c > this.b && this.b + this.c > this.a)) throw new Error(`${this.figureType}. С такими сторонами треугольник не построить`);
    }

    public area(): number {
        let p: number = this.perimeter() / 2;
        return Math.sqrt(p * (p - this.a) * (p - this.b)* (p - this.c));
    }

    public perimeter(): number {
        return this.a + this.b + this.c;
    }
}